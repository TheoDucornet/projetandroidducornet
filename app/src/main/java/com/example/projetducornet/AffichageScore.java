package com.example.projetducornet;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.projetducornet.questionnaireUtils.Questionnaire;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class AffichageScore extends AppCompatActivity {

    List<String> resultats = new ArrayList<>();

    private ListView lv;

    private ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_affichage_score);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar4);
        setSupportActionBar(myToolbar);

        lv = (ListView) findViewById(R.id.listeResultats);
        getResults();
        adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1,
                resultats);
        lv.setAdapter(adapter);
    }

    private void getResults() {
        File saveFile = new File(getExternalFilesDir(null), MainActivity.SAVEFILE);

        try {
            // Lis le contenu du fichier save
            BufferedReader reader = new BufferedReader(new FileReader(saveFile));

            String theme;
            int sommeScore = 0;
            int sommeScoreMax = 0;
            // Crée une ligne par thème et résultat
            while ((theme = reader.readLine()) != null) {
                String[] notes = reader.readLine().split("/");
                sommeScore += Integer.parseInt(notes[0].trim());
                sommeScoreMax += Integer.parseInt(notes[1].trim());
                String finalString = theme + " : " + notes[0] + " / " + notes[1];
                System.out.println(finalString);
                resultats.add(finalString);
                 }
            // Affiche la moyenne
            if(sommeScoreMax != 0) {
                TextView moyenne = (TextView) findViewById(R.id.noteTextView);
                DecimalFormat decimalFormat = new DecimalFormat("#0.00");
                String moyenneText = decimalFormat.format((double) sommeScore / sommeScoreMax * 20);
                moyenne.setText(moyenneText + " / 20");
            }

            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void clickOK(View view) {
        finish();
    }
}