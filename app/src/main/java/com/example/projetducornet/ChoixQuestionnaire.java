package com.example.projetducornet;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.projetducornet.questionnaireUtils.Question;
import com.example.projetducornet.questionnaireUtils.Questionnaire;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class ChoixQuestionnaire extends AppCompatActivity {

    public static final String QUESTIONNAIRE = "questionnaire";

    List<Questionnaire> questionnaires = new ArrayList<>();

    List<String> questionnairesLibelles = new ArrayList<>();

    private ListView lv;

    private ArrayAdapter<String> adapter;

    Questionnaire selectedQuestionnaire;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choix_questionnaire);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar3);
        setSupportActionBar(myToolbar);

        // Récupération des questionnaires pré-enregistrés
        getBasicQuestionnaires();

        // Récupération des questionnaires créés par l'utilisateur
        getCustomQuestionnaires();


        // Retire les questionnaires déjà faits
        removeAlreadyDoneQuestionnaire();

        // Affichage des questionnaires
        lv = (ListView) findViewById(R.id.liste);
        updateQuestionnairesLibelles();
        adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1,
                questionnairesLibelles);
        lv.setAdapter(adapter);

        // Sélection d'un questionnaire
        lv.setOnItemClickListener((parent, view, position, id) -> {
            resetListViewBackground();
            view.setBackgroundColor(Color.LTGRAY);
            selectedQuestionnaire = questionnaires.get(position);
            Toast.makeText(getApplicationContext(), "Sélection de " + selectedQuestionnaire.getTheme(), Toast.LENGTH_SHORT).show();
        });

    }

    @Override
    protected void onResume() {
        removeAlreadyDoneQuestionnaire();
        updateQuestionnairesLibelles();
        adapter.notifyDataSetChanged();
        resetListViewBackground();
        super.onResume();
    }

    private void updateQuestionnairesLibelles() {
        questionnairesLibelles.clear();
        questionnaires.forEach(questionnaire -> questionnairesLibelles.add(questionnaire.getTheme()));
    }

    private void resetListViewBackground() {
        int count = lv.getCount();
        for (int i = 0; i < count; i++) {
            View view = lv.getChildAt(i);
            if (view != null) {
                view.setBackgroundColor(Color.TRANSPARENT);
            }
        }
    }

    public void onClickQuestionnaire(View view) {
        if (selectedQuestionnaire != null) {
            Intent mess = new Intent(this, ReponseQuestion.class);
            selectedQuestionnaire.shuffleQuestions();
            mess.putExtra(QUESTIONNAIRE, selectedQuestionnaire);
            selectedQuestionnaire = null;
            startActivity(mess);
        } else {
            Toast.makeText(getApplicationContext(), "Veuillez sélectionner un questionnaire", Toast.LENGTH_SHORT).show();
        }
    }

    private void removeAlreadyDoneQuestionnaire() {
        File saveFile = new File(getExternalFilesDir(null), MainActivity.SAVEFILE);

        try {
            // Lis le contenu du fichier save et retire les thèmes présents
            BufferedReader reader = new BufferedReader(new FileReader(saveFile));

            String theme;
            while ((theme = reader.readLine()) != null) {
                String finalTheme = theme;
                System.out.println(theme);
                questionnaires.stream()
                        .filter(questionnaire -> questionnaire.getTheme().equals(finalTheme)).findFirst().ifPresent(questionnaires::remove);
            }
            reader.close();

        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    private void getBasicQuestionnaires() {
        int[] rawFileIds = {R.raw.qcm01, R.raw.qcm02, R.raw.qcm3};

        for (int rawFileId : rawFileIds) {
            Questionnaire questionnaire = readQuestionnaireFromRawFile(rawFileId);
            if (questionnaire != null) {
                questionnaires.add(questionnaire);
            }
        }
    }

    private Questionnaire readQuestionnaireFromRawFile(int rawFileId) {
        InputStream inputStream = getResources().openRawResource(rawFileId);
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        return readQuestionnaire(reader);
    }

    private void getCustomQuestionnaires() {
        File folder = new File(getExternalFilesDir(null), "questionnaire");
        File[] files = folder.listFiles();
        if (files == null) {
            System.out.println("Pas de fichier custom");
            return;
        }

        for (File file : files) {
            System.out.println(file.getName());
            try {
                Questionnaire questionnaire = readQuestionnaireFromCustomFile(file);
                if (questionnaire != null) {
                    questionnaires.add(questionnaire);
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Toast.makeText(this, "Erreur lors de la lecture du fichier : " + file.getName(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private Questionnaire readQuestionnaireFromCustomFile(File file) throws FileNotFoundException {
        BufferedReader reader = new BufferedReader(new FileReader(file));
        return readQuestionnaire(reader);
    }

    private Questionnaire readQuestionnaire(BufferedReader reader) {
        try {
            String categorie = reader.readLine();
            System.out.println(categorie);
            Questionnaire questionnaire = new Questionnaire(categorie);

            String libelleQuestion;
            while ((libelleQuestion = reader.readLine()) != null) {
                if (libelleQuestion.equals("")) {
                    continue;
                }
                System.out.println(libelleQuestion);
                List<String> choix = new ArrayList<>();
                int correctAnswerIndex = 0;
                for (int i = 0; i < 3; i++) {
                    String newline = reader.readLine();
                    if (newline == null) {
                        break;
                    }
                    String contenu = newline.trim();
                    if (!contenu.isEmpty()) {
                        if (contenu.endsWith(" x")) {
                            choix.add(contenu.substring(0, contenu.length() - 1));
                            correctAnswerIndex = i;
                        } else {
                            choix.add(contenu);
                        }
                    } else {
                        i--;
                    }
                }

                System.out.println(choix);
                System.out.println(correctAnswerIndex);

                Question question = new Question(libelleQuestion, choix, correctAnswerIndex);
                questionnaire.addQuestion(question);
            }

            return questionnaire;

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}