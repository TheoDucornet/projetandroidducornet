package com.example.projetducornet;

import static android.os.SystemClock.sleep;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.projetducornet.questionnaireUtils.Question;
import com.example.projetducornet.questionnaireUtils.Questionnaire;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class ReponseQuestion extends AppCompatActivity {

    int index = 0;

    int score = 0;

    Questionnaire questionnaire;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reponse_question);

        questionnaire = (Questionnaire) getIntent().getSerializableExtra(ChoixQuestionnaire.QUESTIONNAIRE);

        TextView theme = findViewById(R.id.ThemeTextView);
        theme.setText(questionnaire.getTheme());

        displayQuestion();
    }

    private void displayQuestion() {
        Question question = questionnaire.getQuestions().get(index);

        TextView indexQuestion = findViewById(R.id.IndexQuestionTextView);
        indexQuestion.setText("Question " + (index + 1) + " / " + questionnaire.getQuestions().size());

        TextView questionTextView = findViewById(R.id.QuestionTextView);
        questionTextView.setText(question.getQuestion());

        RadioButton reponse1 = findViewById(R.id.reponse1);
        reponse1.setText(question.getChoices().get(0));

        RadioButton reponse2 = findViewById(R.id.reponse2);
        reponse2.setText(question.getChoices().get(1));

        RadioButton reponse3 = findViewById(R.id.reponse3);
        reponse3.setText(question.getChoices().get(2));
    }

    public void valider(View view) {
        RadioGroup radioGroup = findViewById(R.id.reponsesRadioGroup);
        int selectedId = radioGroup.getCheckedRadioButtonId();
        if (selectedId == -1) {
            Toast.makeText(this, "Veuillez sélectionner une réponse !", Toast.LENGTH_SHORT).show();
            return;
        }

        // Sélection de l'index de la réponse donnée
        int answer = -1;
        if (selectedId == R.id.reponse1) {
            answer = 0;
        } else if (selectedId == R.id.reponse2) {
            answer = 1;
        } else if (selectedId == R.id.reponse3) {
            answer = 2;
        }

        // Vérification de la réponse
        System.out.println("Réponse attendue : " + questionnaire.getQuestions().get(index).getCorrectAnswerIndex());
        System.out.println("Réponse donnée : " + answer);
        if(answer == questionnaire.getQuestions().get(index).getCorrectAnswerIndex()){
            score++;
        }

        // Passage à la question suivante
        index++;
        if (index < questionnaire.getQuestions().size()) {
            displayQuestion();
        } else if (index == questionnaire.getQuestions().size()) {
            // Fin du questionnaire
            Toast.makeText(this, "Score : " + score + " / " + questionnaire.getQuestions().size(), Toast.LENGTH_SHORT).show();
            saveScore();
            finish();
        }
    }

    private void saveScore() {
        File saveFile = new File(getExternalFilesDir(null), MainActivity.SAVEFILE);

        try (FileWriter fw = new FileWriter(saveFile.getAbsoluteFile(), true)) {
            fw.write(questionnaire.getTheme() + "\n");
            fw.write(score + " / " + questionnaire.getQuestions().size() + "\n");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void stopper(View view) {

        Toast.makeText(this, "Arret du quizz et mise du score à 0", Toast.LENGTH_SHORT).show();

        score = 0;

        File saveFile = new File(getExternalFilesDir(null), MainActivity.SAVEFILE);

        try (FileWriter fw = new FileWriter(saveFile.getAbsoluteFile(), true)) {
            fw.write(questionnaire.getTheme() + "\n");
            fw.write(score + " / " + questionnaire.getQuestions().size() + "\n");

        } catch (IOException e) {
            e.printStackTrace();
        }

        finish();
    }
}