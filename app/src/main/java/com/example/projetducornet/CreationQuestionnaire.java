package com.example.projetducornet;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.projetducornet.questionnaireUtils.Question;
import com.example.projetducornet.questionnaireUtils.Questionnaire;
import com.google.android.material.textfield.TextInputEditText;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CreationQuestionnaire extends AppCompatActivity {

    private List<Question> questions = new ArrayList<>();

    TextInputEditText questionText;

    TextInputEditText reponse1Text;

    TextInputEditText reponse2Text;

    TextInputEditText reponse3Text;

    RadioGroup radioGroup;

    TextInputEditText themeText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_creation_questionnaire);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar5);
        setSupportActionBar(myToolbar);

        questionText = findViewById(R.id.questionInput);
        reponse1Text = findViewById(R.id.reponse1Input);
        reponse2Text = findViewById(R.id.reponse2Input);
        reponse3Text = findViewById(R.id.reponse3Input);
        radioGroup = findViewById(R.id.radioGroup);
        themeText = findViewById(R.id.themeInput);
    }

    public void clickOnSaveQuestion(View view) {
        if (questionText.getText() == null || questionText.getText().toString().isEmpty() ||
                reponse1Text.getText() == null || reponse1Text.getText().toString().isEmpty() ||
                reponse2Text.getText() == null || reponse2Text.getText().toString().isEmpty() ||
                reponse3Text.getText() == null || reponse3Text.getText().toString().isEmpty() ||
                radioGroup.getCheckedRadioButtonId() == -1) {
            Toast.makeText(this, "Veuillez remplir tous les champs", Toast.LENGTH_SHORT).show();
            return;
        }

        String questionLibelle = questionText.getText().toString();
        String reponse1 = reponse1Text.getText().toString();
        String reponse2 = reponse2Text.getText().toString();
        String reponse3 = reponse3Text.getText().toString();

        int selectedId = radioGroup.getCheckedRadioButtonId();

        int correctAnswerIndex = -1;
        if (selectedId == R.id.radioButton1) {
            correctAnswerIndex = 0;
        } else if (selectedId == R.id.radioButton2) {
            correctAnswerIndex = 1;
        } else if (selectedId == R.id.radioButton3) {
            correctAnswerIndex = 2;
        }

        List<String> reponses = new ArrayList<>();
        reponses.add(reponse1);
        reponses.add(reponse2);
        reponses.add(reponse3);

        System.out.println("Question : " + questionLibelle);
        System.out.println("Reponse 1 : " + reponse1);
        System.out.println("Reponse 2 : " + reponse2);
        System.out.println("Reponse 3 : " + reponse3);
        System.out.println("Correct answer index : " + correctAnswerIndex);

        Question question = new Question(questionLibelle, reponses, correctAnswerIndex);
        questions.add(question);

        resetQuestionView();
    }

    public void clickOnSaveQuestionnaire(View view) {
        if (themeText.getText() == null || themeText.getText().toString().isEmpty()) {
            Toast.makeText(this, "Veuillez ecrire un theme", Toast.LENGTH_SHORT).show();
            return;
        }
        if (questions.isEmpty()) {
            Toast.makeText(this, "Veuillez ajouter au moins une question", Toast.LENGTH_SHORT).show();
            return;
        }

        // Création du questionnaire
        String theme = themeText.getText().toString();
        Questionnaire questionnaire = new Questionnaire(theme, questions);

        // Sauvegarde du questionnaire
        writeQuestionnaire(questionnaire);

        finish();
    }

    private void resetQuestionView() {
        questionText.setText(null);
        reponse1Text.setText(null);
        reponse2Text.setText(null);
        reponse3Text.setText(null);
        radioGroup.clearCheck();
    }
    private void writeQuestionnaire(Questionnaire questionnaire) {

        // Crée le dossier "questionnaire" s'il n'existe pas
        File folder = new File(getExternalFilesDir(null), "questionnaire");
        if (!folder.exists()) {
            folder.mkdir();
        }

        // Crée le nom du fichier
        String theme = questionnaire.getTheme();
        String fileName = theme + ".txt";
        File file = new File(folder, fileName);

        // Vérifie si le fichier existe déjà
        if (file.exists()) {
            Toast.makeText(this, "Erreur : Le fichier " + fileName + " existe déjà.", Toast.LENGTH_SHORT).show();
            return;
        }

        // Crée le fichier
        try {
            boolean created = file.createNewFile();
            if (created) {
                System.out.println("Le fichier " + fileName + " a été créé.");
            } else {
                Toast.makeText(this, "Erreur lors de la création du fichier : " + fileName, Toast.LENGTH_SHORT).show();
            }
        } catch (IOException e) {
            Toast.makeText(this, "Erreur lors de la création du fichier : " + fileName, Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }

        try (FileWriter writer = new FileWriter(file)) {
            writer.write(theme + System.lineSeparator());
            writer.write(System.lineSeparator());

            for (Question question : questionnaire.getQuestions()) {
                writer.write(question.getQuestion() + System.lineSeparator());
                writer.write(System.lineSeparator());
                for (int i = 0; i < question.getChoices().size(); i++) {
                    String choice = question.getChoices().get(i);
                    if (i == question.getCorrectAnswerIndex()) {
                        choice = choice + " x";
                    }
                    writer.write(choice + System.lineSeparator());
                }
                writer.write(System.lineSeparator());
            }

            System.out.println("Le questionnaire a été écrit dans le fichier : " + fileName);
            Toast.makeText(this, "Le questionnaire a été écrit dans le fichier : " + fileName, Toast.LENGTH_SHORT).show();

        } catch (IOException e) {
            System.err.println("Erreur lors de l'écriture dans le fichier : " + fileName);
            Toast.makeText(this, "Erreur lors de l'écriture dans le fichier : " + fileName, Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }

    }
}