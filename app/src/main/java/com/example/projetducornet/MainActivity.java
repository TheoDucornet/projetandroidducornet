package com.example.projetducornet;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

import java.io.File;
import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    public final static String SAVEFILE = "save.txt";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);

        createSaveFileIfNotExists();
    }

    public void goToChoixQuestionnaire(View view){
        Intent intent = new Intent(this, ChoixQuestionnaire.class);
        startActivity(intent);
    }

    public void goToAffichageScore(View view){
        Intent intent = new Intent(this, AffichageScore.class);
        startActivity(intent);
    }

    public void goToCreationQuestionnaire(View view){
        TextInputEditText password = findViewById(R.id.textInputPassword);
        if (password.getText() == null || !password.getText().toString().equals("MDP")) {
            Toast.makeText(this, "Mot de passe incorrect", Toast.LENGTH_SHORT).show();
            return;
        }
        Intent intent = new Intent(this, CreationQuestionnaire.class);
        startActivity(intent);
    }

    public void onClickExit(View view){
        finish();
    }

    public void resetScore(View view){
        File saveFile = new File(getExternalFilesDir(null), SAVEFILE);
        boolean deleted = saveFile.delete();
        if (deleted) {
            Toast.makeText(this, "Score réinitialisé", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Erreur lors de la réinitialisation du score", Toast.LENGTH_SHORT).show();
        }
    }

    private void createSaveFileIfNotExists() {
        File saveFile = new File(getExternalFilesDir(null), SAVEFILE);

        if (!saveFile.exists()) {
            try {
                boolean created = saveFile.createNewFile();
                if (created) {
                } else {
                    Toast.makeText(this, "Erreur lors de la création du fichier de sauvegarde", Toast.LENGTH_SHORT).show();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}