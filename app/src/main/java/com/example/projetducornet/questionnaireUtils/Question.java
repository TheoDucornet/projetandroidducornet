package com.example.projetducornet.questionnaireUtils;

import java.io.Serializable;
import java.util.List;

public class Question implements Serializable {
    private final String question;
    private final List<String> choices;
    private final int correctAnswerIndex;

    public Question(String question, List<String> choices, int correctAnswerIndex) {
        this.question = question;
        this.choices = choices;
        this.correctAnswerIndex = correctAnswerIndex;
    }

    public String getQuestion() {
        return question;
    }

    public List<String> getChoices() {
        return choices;
    }

    public int getCorrectAnswerIndex() {
        return correctAnswerIndex;
    }
}
