package com.example.projetducornet.questionnaireUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Questionnaire implements Serializable {
    private final String theme;
    private final List<Question> questions = new ArrayList<>();

    public Questionnaire(String theme) {
        this.theme = theme;
    }

    public Questionnaire(String theme, List<Question> questions) {
        this.theme = theme;
        this.questions.addAll(questions);
    }

    public String getTheme() {
        return theme;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void addQuestion(Question question) {
        questions.add(question);
    }

    public void shuffleQuestions() {
        java.util.Collections.shuffle(questions);
    }
}