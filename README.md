# Projet Android Ducornet Théo M2 I2L

Ce projet permet de créer une application Android permettant de répondre à des questionnaires, de gérer son score,
et de créer des questionnaires.

## Activité principale

L'activité principale permet de choisir entre les différentes fonctionnalités de l'application: choisir un questionnaire, 
affichage et réinitialisation des scores et création d'un questionnaire.  
Un input permet d'entrer un mot de passe qui est requis pour accéder à la création d'un questionnaire.

Au chargement de cette activité, un fichier `save.txt` est crée si il n'existe pas déjà pour enregistrer les scores. Le bouton
 de réinitialisation des scores permet de vider ce fichier. La présence de ce fichier permet d'assurer la sauvegarde des résultats
même en cas de fermeture de l'application.

## Choix d'un questionnaire

Cet écran affiche dans une liste les différents questionnaires qui n'ont pas encore été complétés. Un bouton en bas de l'écran permet de lancer le questionnaire sélectionné.

L'activité charge tout d'abord les questionnaires pré-enregistrés (dans le dossier `res/raw`) puis charge les questionnaires
créés par l'utilisateur (dans le dossier `questionnaire`). Il retire ensuite de sa liste les questionnaires dont le thème est 
présent dans le fichier `save.txt` (qui contient les scores).

A noter que la liste affichée est constituée des libellés de la liste des questionnaires uniquement. Cette liste est mise
à jour à chaque fois que l'utilisateur revient sur cette activité.

Lors du clic sur le bouton de lancement d'un questionnaire, l'activité de réponse aux questions est lancée, avec en extra
dans l'intention le questionnaire sélectionné dont l'ordre des questions a été mélangé.

### Réponse à un questionnaire

Cette activité affiche le thème du questionnaire et la question en cours avec les 3 réponses possibles sous la forme
de radio buttons. Un bouton 'Valider' permet de passer à la question suivante. Lorsque l'utilisateur a répondu à toutes
les questions, le score est enregistré (et affiché via un  message Toast) et l'activité se termine.  
En cas d'appui sur le bouton Stopper, le score du questionnaire est enregistré à 0.

L'enregistrement du score dans le fichier `save.txt` se fait en ajoutant le thème du questionnaire puis le score sur une
nouvelle ligne.

## Visualisation des scores

Cet écran se compose d'une liste récupérant les scores enregistrés dans le fichier `save.txt` et d'un bouton 'OK' permettant
de terminer l'activité.  
Une note moyenne sur 20 est calculée et affichée en dessous de la liste.

## Création d'un questionnaire

Cette activité permet de créer un questionnaire.  
Elle se compose d'un champ de texte pour le thème du questionnaire, d'un champ de texte pour la question, de 3 champs de 
texte pour les réponses collés à 3 radio buttons permettant de choisir la bonne réponse. 
En bas de l'écran, un bouton permet d'enregistrer une question et un autre bouton permet de sauvegarder le questionnaire.

Lors du clic sur la sauvegarde d'une question, si tous les champs sont remplis, la question est ajoutée à une liste de 
questions. Les champs sont ensuite vidés pour permettre à l'utilisateur de saisir une nouvelle question.

Lors du clic sur la sauvegarde du questionnaire, si le thème est rempli et qu'il y a au moins une question, le questionnaire
est enregistré dans le dossier `questionnaire` sous la forme d'un fichier txt dont le nom est le thème du questionnaire.  
Le format du fichier crée respecte le format des fichiers fournis dans le dossier `res/raw`.
